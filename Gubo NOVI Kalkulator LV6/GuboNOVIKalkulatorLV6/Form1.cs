﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GuboNOVIKalkulatorLV6
{
    public partial class Form1 : Form
    {
        double rezultat = 0;
        string operationperformed = "";
        bool isoperationperformed = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void button41_Click(object sender, EventArgs e)
        {
            textBox2.Clear();
            rezultat = 0;
            //vrijednost = 0;
        }

        private void ButtonClick(object sender, EventArgs e)
        {
            /*if (textBox2.Text == "0")
           {
               textBox2.Clear();
           }
           Button button = (Button)sender;
           //textBox2.Text = textBox2.Text + button.Text;
           isoperationperformed = false;

           if (button.Text == ",")
           {
               if (!textBox2.Text.Contains(","))
                   textBox2.Text = textBox2.Text + button.Text;

           }
           else
           {
               textBox2.Text = textBox2.Text + button.Text;
           }*/
            if ((textBox2.Text == "0") || (isoperationperformed)) // u proslom komentaru je falio is operationperformed, inace nakon odabira neke moguce funkcije kalkulatora vrijednost bi ostala na ekranu te bi se samo nadodala nova znamenka
            {
                textBox2.Clear();
            }

            Button button = (Button)sender;
            if (button.Text == ",")
            {
                if (!textBox2.Text.Contains(","))
                    textBox2.Text = textBox2.Text + button.Text;

            }
            else
            {
                textBox2.Text = textBox2.Text + button.Text;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void OperatorClick(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            operationperformed = button.Text;
            rezultat = double.Parse(textBox2.Text);
            // labeltrenutnaoperacija.Text = rezultat + " " + operationperformed;
            isoperationperformed = true;
        }

        private void JednakoClick(object sender, EventArgs e)
        {
            switch (operationperformed)
            {
                case "+":
                    textBox2.Text = (rezultat + double.Parse(textBox2.Text)).ToString();
                    break;
                case "-":
                    textBox2.Text = (rezultat - double.Parse(textBox2.Text)).ToString();
                    break;
                case "*":
                    textBox2.Text = (rezultat * double.Parse(textBox2.Text)).ToString();
                    break;
                case "/":
                    textBox2.Text = (rezultat / double.Parse(textBox2.Text)).ToString();
                    break;
                default:
                    break;

            }
            isoperationperformed = false;
        }

        private void SinButton(object sender, EventArgs e)
        {
            double variable = double.Parse(textBox2.Text);
            variable = Math.Sin(variable);
            textBox2.Text = System.Convert.ToString(variable);
        }

        private void CosButton(object sender, EventArgs e)
        {
            double variable = double.Parse(textBox2.Text);
            variable = Math.Cos(variable);
            textBox2.Text = System.Convert.ToString(variable);
        }

        private void SqrButton(object sender, EventArgs e)
        {
            double variable = double.Parse(textBox2.Text);
            variable =(variable*variable);
            textBox2.Text = System.Convert.ToString(variable);
        }

        private void SqrtButton(object sender, EventArgs e)
        {
            double variable = double.Parse(textBox2.Text);
            variable =Math.Sqrt(variable);
            textBox2.Text = System.Convert.ToString(variable);
        }

        private void LogButton(object sender, EventArgs e)
        {
            double variable = double.Parse(textBox2.Text);
            variable = Math.Log10(variable);
            textBox2.Text = System.Convert.ToString(variable);
        }
    }
 }

