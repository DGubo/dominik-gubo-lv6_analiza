﻿namespace GuboNOVIKalkulatorLV6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(12, 23);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(314, 20);
            this.textBox2.TabIndex = 0;
            this.textBox2.Text = "0";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(12, 105);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(99, 67);
            this.button21.TabIndex = 1;
            this.button21.Text = "7";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.ButtonClick);
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(126, 105);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(99, 67);
            this.button22.TabIndex = 2;
            this.button22.Text = "8";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.ButtonClick);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(241, 105);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(99, 67);
            this.button23.TabIndex = 3;
            this.button23.Text = "9";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.ButtonClick);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(241, 183);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(99, 67);
            this.button24.TabIndex = 6;
            this.button24.Text = "6";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.ButtonClick);
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(126, 183);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(99, 67);
            this.button25.TabIndex = 5;
            this.button25.Text = "5";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.ButtonClick);
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(12, 183);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(99, 67);
            this.button26.TabIndex = 4;
            this.button26.Text = "4";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.ButtonClick);
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(241, 261);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(99, 67);
            this.button27.TabIndex = 9;
            this.button27.Text = "3";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.ButtonClick);
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(126, 261);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(99, 67);
            this.button28.TabIndex = 8;
            this.button28.Text = "2";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.ButtonClick);
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(12, 261);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(99, 67);
            this.button29.TabIndex = 7;
            this.button29.Text = "1";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.ButtonClick);
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(12, 334);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(99, 67);
            this.button30.TabIndex = 10;
            this.button30.Text = "0";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.ButtonClick);
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(120, 334);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(144, 67);
            this.button31.TabIndex = 11;
            this.button31.Text = "=";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.JednakoClick);
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(12, 61);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(30, 24);
            this.button32.TabIndex = 12;
            this.button32.Text = "+";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.OperatorClick);
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(48, 61);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(30, 24);
            this.button33.TabIndex = 13;
            this.button33.Text = "-";
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.OperatorClick);
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(84, 61);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(30, 24);
            this.button34.TabIndex = 14;
            this.button34.Text = "*";
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.OperatorClick);
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(120, 61);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(30, 24);
            this.button35.TabIndex = 15;
            this.button35.Text = "/";
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.OperatorClick);
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(156, 61);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(30, 24);
            this.button36.TabIndex = 16;
            this.button36.Text = "sin";
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.SinButton);
            // 
            // button37
            // 
            this.button37.Location = new System.Drawing.Point(192, 61);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(33, 24);
            this.button37.TabIndex = 17;
            this.button37.Text = "cos";
            this.button37.UseVisualStyleBackColor = true;
            this.button37.Click += new System.EventHandler(this.CosButton);
            // 
            // button38
            // 
            this.button38.Location = new System.Drawing.Point(231, 61);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(33, 24);
            this.button38.TabIndex = 18;
            this.button38.Text = "^2";
            this.button38.UseVisualStyleBackColor = true;
            this.button38.Click += new System.EventHandler(this.SqrButton);
            // 
            // button39
            // 
            this.button39.Location = new System.Drawing.Point(270, 61);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(33, 24);
            this.button39.TabIndex = 19;
            this.button39.Text = "√";
            this.button39.UseVisualStyleBackColor = true;
            this.button39.Click += new System.EventHandler(this.SqrtButton);
            // 
            // button40
            // 
            this.button40.Location = new System.Drawing.Point(310, 61);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(33, 24);
            this.button40.TabIndex = 20;
            this.button40.Text = "log";
            this.button40.UseVisualStyleBackColor = true;
            this.button40.Click += new System.EventHandler(this.LogButton);
            // 
            // button41
            // 
            this.button41.Location = new System.Drawing.Point(275, 334);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(65, 67);
            this.button41.TabIndex = 21;
            this.button41.Text = "C";
            this.button41.UseVisualStyleBackColor = true;
            this.button41.Click += new System.EventHandler(this.button41_Click);
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(355, 419);
            this.Controls.Add(this.button41);
            this.Controls.Add(this.button40);
            this.Controls.Add(this.button39);
            this.Controls.Add(this.button38);
            this.Controls.Add(this.button37);
            this.Controls.Add(this.button36);
            this.Controls.Add(this.button35);
            this.Controls.Add(this.button34);
            this.Controls.Add(this.button33);
            this.Controls.Add(this.button32);
            this.Controls.Add(this.button31);
            this.Controls.Add(this.button30);
            this.Controls.Add(this.button27);
            this.Controls.Add(this.button28);
            this.Controls.Add(this.button29);
            this.Controls.Add(this.button24);
            this.Controls.Add(this.button25);
            this.Controls.Add(this.button26);
            this.Controls.Add(this.button23);
            this.Controls.Add(this.button22);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.textBox2);
            this.Name = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button41;
    }
}

